function palindrome(str) {
    const cleanStr = str.replace(/[^A-Za-z0-9]/g, '').toLowerCase();
    const reversedStr = cleanStr.split("").reverse().join('');
    return (cleanStr === reversedStr) ? true : false;
}

console.log(palidrome("eye"));
console.log(palidrome("A man, a plan, a canal. Panama"));